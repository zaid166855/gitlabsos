1.2
====
 - Works around a rare race condition ([#75](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/-/issues/75): `No such file or directory @ rb_file_s_mtime` due to a file being removed while the SOS script runs) by skipping those.

1.0
=====

 - Add version control
